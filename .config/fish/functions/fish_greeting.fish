function fish_greeting
        read -x HOSTNAME < /etc/hostname
        set HOSTIMG "$HOME/.config/misc/$HOSTNAME.jpg"
        figlet -f basic $HOSTNAME
        if test $TERM = 'xterm-kitty'
          kitty +kitten icat --align=left {$HOSTIMG}
        end
        cat ~/.config/misc/weather.txt 
end
