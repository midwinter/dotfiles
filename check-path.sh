#!/bin/bash

USER_LOCAL=~/.local
USER_BIN=$USER_LOCAL/bin

if ! echo "$PATH" | /bin/grep -Eq "(^|:)$USER_BIN($|:)" ; then
      exit 1
fi
exit 0
