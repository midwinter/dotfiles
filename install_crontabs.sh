#!/bin/bash
FILES=$1/*

for f in $FILES
do
    if [ `crontab -l | grep $f` ]; then
        echo "crontab $f already installed"
    else
        echo "Installing $f"
        { crontab -l; echo "#$f"; cat $f; } | crontab -
    fi
done

