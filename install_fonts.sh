#!/bin/bash
function install_font {

    echo "Installing $1"
    cd /tmp
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/$1.zip
    unzip $1.zip -d $1
    cd $1
    mv ./* ~/.fonts

}

install_font "FiraCode"
install_font "Ubuntu"
install_font "UbuntuMono"
install_font "Monofur"
install_font "SourceCodePro"
install_font "Terminus"
fc-cache -fv
