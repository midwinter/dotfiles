#!/bin/bash

TEMP_DIR=~/tmp
USER_LOCAL=~/.local
USER_BIN=$USER_LOCAL/bin
CUR_DIR=`pwd`

function create_dir () {

	if [ -d $1 ]; then
		echo "$1 exists not creating."
	else
		echo "$1 doesn't exist. Creating."
		mkdir $1
	fi
}

echo "Creating Directories"
create_dir $TEMP_DIR
create_dir $USER_LOCAL
create_dir $USER_BIN

echo ""
echo "Adding local bin to path"

if ! echo "$PATH" | /bin/grep -Eq "(^|:)$USER_BIN($|:)" ; then
      export PATH="$USER_BIN:$PATH"
fi
echo $PATH

echo ""
echo "Installing Homemaker"
cd $TEMP_DIR
wget https://foosoft.net/projects/homemaker/dl/homemaker_linux_amd64.tar.gz
tar xvf homemaker_linux_amd64.tar.gz
cp homemaker_linux_amd64/homemaker $USER_BIN/

echo "Homemaker installed to $USER_BIN/homemaker"
#echo "You may want to add $USER_BIN to your path"
#echo "export PATH=${USER_BIN}:\$PATH"
cd $CUR_DIR
echo `pwd`
# homemaker homemaker.json ${CUR_DIR}

