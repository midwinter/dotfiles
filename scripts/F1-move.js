#!/usr/bin/env node
const fs = require("fs");
const path = require("path");

const filenameRegex = /^.+([1-2][0-9]{3})\.R([0-9]{2})\.([a-zA-Z\.]+)1080.+mkv$/i;
const sourceFolder = "/home/craig/tmp/F1-test/src";
const destinationFolder = "/home/craig/tmp/F1-test/dest";

function fileDetails(file) {
  const match = matchFile(file);
  const episode = parseEpisode(match[3]);
  const episodeFilename = `${episode} - ${match[3]
    .replace(/\./g, " ")
    .trim()}.mkv`;
  return {
    filePath: match[0],
    series: match[1],
    season: match[2],
    episodeFilename
  };
}

function processFile(file) {
  const { filePath, series, season, episodeFilename } = fileDetails(file);
  const fullDestDir = [destinationFolder, series, season].join("/");
  const fullDestFilepath = [fullDestDir, episodeFilename].join("/");
  fs.mkdirSync(fullDestDir, { recursive: true });
  try {
    console.log(`Linking ${filePath} to ${fullDestFilepath}`)
    fs.linkSync(filePath, fullDestFilepath);
  } catch (e) {
    console.log(e);
    console.log("Failed, copying")
    fs.copyFileSync(filePath, fullDestFilepath);
  }
}

function parseEpisode(filename) {
  const qualifyingRegex = /qualifying/i;
  const preRaceRegex = /pre\.race/i;
  const postRaceRegex = /post\.race/i;
  if (filename.match(qualifyingRegex)) {
    return "01";
  }
  if (filename.match(preRaceRegex)) {
    return "02";
  }
  if (filename.match(postRaceRegex)) {
    return "04";
  }
  return "03";
}

function processCompletedFiles() {
  const files = [];
  walk(sourceFolder, (err, files) => {
    for (const file of files) {
      if (needsProcessing(file)) {
        console.log(file);
        processFile(file);
      }
    }
  });
}

function needsProcessing(file) {
  return matchFile(file) != null;
  console.log(file);
  const result = console.log(JSON.stringify(result, null, 2));
}

function matchFile(file) {
  return file.match(filenameRegex);
}

function processFiles() {
  processCompletedFiles();
}

function walk(dir, done) {
  var results = [];
  const list = fs.readdirSync(dir); //, function(err, list) {
  var i = 0;
  (function next() {
    var file = list[i++];
    if (!file) return done(null, results);
    file = path.resolve(dir, file);
    fs.stat(file, function(err, stat) {
      if (stat && stat.isDirectory()) {
        walk(file, function(err, res) {
          results = results.concat(res);
          next();
        });
      } else {
        results.push(file);
        next();
      }
    });
  })();
}

processFiles();
