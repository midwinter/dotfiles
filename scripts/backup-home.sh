#!/bin/bash
BACKUPTIME=`date +%b-%d-%y` #get the current date
YESTERDAYBACKUPTIME=`date -d "yesterday 13:00" +%b-%d-%y` #get the current date
DESTINATION=/home/craig/backups/backup-andoria-home-$BACKUPTIME.tar.gz #create a backup file using the current date in it's name
YESTERDAYDESTINATION=/home/craig/backups/backup-andoria-home-$YESTERDAYBACKUPTIME.tar.gz #create a backup file using the current date in it's name
SOURCEFOLDER=/home/craig #the folder that contains the files that we want to backup

rm -rf /home/craig/backups/*
echo "Home Files"
echo "compressing backup"
tar --exclude-from=/home/craig/scripts/home_backup_skip -cpzf $DESTINATION $SOURCEFOLDER #create the backup
echo "done. uploading"
/home/craig/.local/bin/aws s3 cp $DESTINATION s3://midwinter-backups --profile goalfeed
echo "kthxbye"

