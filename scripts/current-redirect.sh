#!/bin/bash

exit 0;
ENDPOINT=$(curl -s http://localhost/current-endpoint)
if [ $? == 1 ]; then
        echo -e " \uf26c \uf6cf stopped "
elif [ "$ENDPOINT" = "http://local.espn.com:8080/" ]; then
        echo -e " \uf26c \uf6cf local "
else
        echo -e " \uf26c \uf6cf $ENDPOINT "
fi
