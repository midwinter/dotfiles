#!/usr/bin/babel-node --presets=latest

function sendMsg(msg) {

    try { 
    const WebSocket = require('ws');
    const ws = new WebSocket('ws://nodered.local/ws/meeting');
    ws.on('open', function open() {
        ws.send(msg);
    });
    } catch (e) { 

        console.log('socket error');
    }
}

let inMeeting = null;



function isCameraRunning () {
    const  exec = require('child_process').exec;
    const cmd = "../on-air.sh"

    return new Promise(function (resolve, reject) {
        exec(cmd, (err, stdout, stderr) => {
            if(stdout.trim() == 'off air') {
                resolve(false);
            } else {
                resolve(true);
            }
        });
    });

}

async function checkMeeting() {

    let cameraRunning = await isCameraRunning();
    console.log('checking meeting');
    console.log(cameraRunning);
    if(cameraRunning !== inMeeting) {

        console.log('meeting state changed');
        inMeeting = cameraRunning;
        const msg = inMeeting ? 'meeting.started' : 'meeting.finished';
        sendMsg(msg);

    }
}
setInterval(() => {checkMeeting()}, 2000);

//checkMeeting();
