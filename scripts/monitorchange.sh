#!/bin/bash

xrandr -q  | grep -q -i 'DP-2-2 disconnected'

if [ $? -eq 0  ]; then
    ~/.screenlayout/no-mon.sh
else
    ~/.screenlayout/all-mon.sh
fi
~/.config/polybar/launch.sh
