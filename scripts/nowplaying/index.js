const fs = require("fs");

const settings = {
  playbackPath:
    "/home/craig/.config/Google Play Music Desktop Player/json_store/playback.json",
  outputFormat: " :artist: - :title: (:current_time:/:total_time:)"
  //" :artist: - :title: :remaining_time: (:current_time:/:total_time:)"
};

const loadJsonFile = (path, opts = "utf8") =>
  new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) reject(err);
      else resolve(JSON.parse(data));
    });
  });

function msToTime(duration, opts = {}) {
  const {
    includeMilliseconds = false,
    zeroHours = "",
    zeroMinutes = "00:",
    zeroSeconds = "00"
  } = opts;

  const milliseconds = parseInt((duration % 1000) / 100);
  let seconds = Math.floor((duration / 1000) % 60);
  let minutes = Math.floor((duration / (1000 * 60)) % 60);
  let hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  const hoursString = hours > 0 ? hours + ":" : zeroHours;
  const minutesString = minutes > 0 ? minutes + ":" : zeroMinutes;
  const secondsString = seconds > 0 ? seconds : zeroSeconds;
  const millisecondsString = includeMilliseconds ? "." + milliseconds : "";

  return hoursString + minutesString + secondsString + millisecondsString;
}

run = async () => {
  const data = await loadJsonFile(settings.playbackPath);

  if (!data || !data.playing) {
    //console.log("  Stopped");
    console.log(" ");
    return;
  }

  let output = settings.outputFormat;

  const substitutions = {
    title: data.song.title,
    artist: data.song.artist,
    current_time: msToTime(data.time.current),
    total_time: msToTime(data.time.total),
    remaining_time: msToTime(data.time.total - data.time.current, {
      zeroMinutes: ""
    })
  };

  for (const [key, val] of Object.entries(substitutions)) {
    let replaceKey = ":" + key + ":";
    output = output.replace(replaceKey, val);
  }
  console.log(output);
};

run();
