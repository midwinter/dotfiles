#!/bin/bash
STATUS=`cat $HOME/scripts/dark-status`
if [ "$STATUS" == "dark" ]
then
    newstatus="light"
else
    newstatus="dark"
fi

echo $newstatus > $HOME/scripts/dark-status
$HOME/scripts/wal.sh
