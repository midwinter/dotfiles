#!/bin/sh

NORD_IFACE=$(ifconfig | grep tun | awk '{print $1}')
BAM_IFACE=$(ifconfig | grep tun | awk '{print $1}')

if [ "$BAM_IFACE" = "cscotun0:" ]; then
    echo " 輦 BAMtech %{u#55aa55}$(ifconfig cscotun0 | grep inet | awk '{print $2}' | cut -f2 -d ':')%{u-} "
elif [ "$NORD_IFACE" = "tun0:" ]; then
    echo " 輦 Nord %{u#55aa55}$(ifconfig tun0 | grep inet | awk '{print $2}' | cut -f2 -d ':')%{u-} "
else
    echo  " 輦  "
fi
