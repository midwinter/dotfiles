#!/bin/bash
STATUS=`cat $HOME/scripts/dark-status`
if [ "$STATUS" == "dark" ]
then
    wal -i ~/wallpaper/dark/ #-a 80
else
    wal -i ~/wallpaper/light/ -l #-a 80 
fi
