#!/bin/bash
PATH=/home/craig/tizen-studio/tools/ide/bin/:/usr/java/jre1.8.0_211/bin/:/home/craig/WebStorm/bin:/home/craig/.local/kitty.app/bin:/home/craig/.local/bin:/home/craig/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/craig/.dotnet/tools
DIR="/home/$USER/wallpapers"
PIC=$(ls $DIR/* | shuf -n 1)
wal -a 65 -i $PIC
pywal-hue-hook.py
feh --bg-fill "file://$PIC"
