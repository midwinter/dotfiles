#!/bin/bash
arch=$(uname -i)
current_dir=$(pwd)
tmp_dir=$(mktemp -d)
cd $tmp_dir
install() {
    packagesNeeded=$@
    if [ -x "$(command -v apk)" ];       then sudo apk add -y --no-cache $packagesNeeded
    elif [ -x "$(command -v apt)" ]; then sudo apt update; sudo apt install -y $packagesNeeded
    else echo "FAILED TO INSTALL PACKAGE: Package manager not found. You must manually install"
    fi
}
echo "Installing dependencies"


install gcc curl wget figlet

#echo "Installing ruby"
#wget -O ruby-install-0.8.2.tar.gz https://github.com/postmodern/ruby-install/archive/v0.8.2.tar.gz
#tar -xzvf ruby-install-0.8.2.tar.gz
#cd ruby-install-0.8.2/
#sudo make install
#ruby-install 2.6.3

#echo "Installing brew"
#git clone https://github.com/Homebrew/brew ~/.linuxbrew/Homebrew
#mkdir ~/.linuxbrew/bin

#ln -s ~/.linuxbrew/Homebrew/bin/brew ~/.linuxbrew/bin
#eval $(~/.linuxbrew/bin/brew shellenv)
#echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> ~/.profile
#eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"


echo "Installing fish"
#brew install fish
git clone https://github.com/fish-shell/fish-shell.git
cd fish-shell; cmake .; make; sudo make install
#fish -c "curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher"
#fish -c "fisher install edc/bass"
#fish -c "fisher install jethrokuan/fzf""


echo "Installing Yadm"
#brew install gcc 
mkdir ~/.bin
echo 'export PATH=~/.bin:$PATH' >> ~/.profile
source ~/.profile
curl -fLo ~/.bin/yadm https://github.com/TheLocehiliosan/yadm/raw/master/yadm && chmod a+x ~/.bin/yadm

yadm clone https://gitlab.com/midwinter/dotfiles.git
yadm fetch
yadm pull

echo "Installing nvim"
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
./nvim.appimage --appimage-extract
./squashfs-root/AppRun --version
mv ./squashfs-root/AppRun ~/.bin/nvim

if [ -e "$HOME/.config/nvim/autoload/plug.vim" ]; then
  echo "VimPlug already installed"
else
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi
echo 'alias vim="nvim"' >> ~/.profile
source ~/.profile

echo "Done. You may need to run:"

echo "Temp $TEMP_DIR"
echo ""
echo "source ~/.profile"
echo ""
cd $current_dir
